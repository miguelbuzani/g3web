﻿using Modbus.Device;
using System;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.AspNetCore.SignalR.Client;
using System.Net.Sockets;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace ModbusClient
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        HubConnection connection;
        DispatcherTimer timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();
            connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44325/TempHub")
                .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };
            timer.Interval = new TimeSpan(0, 0, 10);

        }

        async void OnLoad(object sender, RoutedEventArgs e)
        {
            /*
            this.Dispatcher.Invoke(() =>
            {
                connection.On<string, int[]>("ReceiveHR", (user, message) =>
                {
                    var newMessage = $"{message}";
                    ushort[] hrValues = new ushort[10];
                    for (Int16 i = 0; i < 10; i++)
                        hrValues[i] = (ushort)message[i];
                    WriteInputs(hrValues);
                });

            });
            */


            connection.On<string, int[]>("ReceiveHR", (user, message) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    var newMessage = $"{message}";
                    ushort[] hrValues = new ushort[10];
                    for (int i = 0; i < 10; i++)
                        hrValues[i] = (ushort)message[i];

                    WriteInputs(hrValues);
                });
            });

            try
            {
                await connection.StartAsync();
                MessageBox.Show("Connection started");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nClosing program (Try and have the slave connected)");
                //Environment.Exit(0);
            }
            ReadInputs();
            timer.Tick += new EventHandler(dispatcherTimer_Tick);
            timer.Start();



            //Thread.Sleep(5000);
            //ReadInputs();
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (NumberTextBox1.Text == "") NumberTextBox1.Text = "0";
            if (NumberTextBox2.Text == "") NumberTextBox2.Text = "0";
            if (NumberTextBox3.Text == "") NumberTextBox3.Text = "0";
            timer.Interval = new TimeSpan(Int32.Parse(NumberTextBox1.Text), Int32.Parse(NumberTextBox2.Text), Int32.Parse(NumberTextBox3.Text));
            ReadInputs();

        }

        public async void ReadInputs()
        {


            using (TcpClient client = new TcpClient("127.0.0.1", 502))
            {

                ModbusIpMaster master = ModbusIpMaster.CreateIp(client);
                // read five input values
                ushort startAddress = 0;
                ushort numInputs = 10;
                string hrDisplay = "";
                FlowDocument ObjFdoc = new FlowDocument();
                Paragraph paragraph = new Paragraph();
                ushort[] hrValues = master.ReadHoldingRegisters(startAddress, numInputs);

                for (int i = 0; i < numInputs; i++)
                {
                    hrDisplay += (Convert.ToString(hrValues[i]) + "\n");
                    if (hrValues[i] >= 100)
                    {
                        paragraph.Inlines.Add(new Run(Convert.ToString(hrValues[i] + "\n"))
                        {
                            Foreground = Brushes.Red
                        });
                        paragraph.Inlines.Add(new LineBreak());
                    }
                    else
                    {
                        paragraph.Inlines.Add(new Run(Convert.ToString(hrValues[i] + "\n")) { Foreground = Brushes.Black });
                        paragraph.Inlines.Add(new LineBreak());
                    }
                    ObjFdoc.Blocks.Add(paragraph);
                    rtxtbox1.Document = ObjFdoc;

                }

                try
                {
                    await connection.InvokeAsync("SendMessage", "wea", hrDisplay);
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                    //Environment.Exit(0);
                }

            }


            // output: 
            // Input 100=0
            // Input 101=0
            // Input 102=0
            // Input 103=0
            // Input 104=0
        }
        public void WriteInputs(ushort[] hrValues)
        {
            using (TcpClient client = new TcpClient("127.0.0.1", 502))
            {
                ModbusIpMaster master = ModbusIpMaster.CreateIp(client);

                // read five input values
                //ushort[] hrValues = new ushort[10];
                ushort startAddress = 0;
                //for (ushort i = 0; i < 10; i++)
                //    hrValues[i] = i;
                master.WriteMultipleRegisters(startAddress, hrValues);
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /*
        public void DelegateMethod()
        {
            Int16[] Registers = new Int16[10];
            WSMBT.Result Result;
            connection.On<string, Int16[]>("ReceiveHR", (user, message) =>
            {
                for (Int16 i = 0; i < 10; i++)
                    Registers[i] = message[i];
                Result = wsmbtControl1.WriteMultipleRegisters(1, 0, 10, Registers);
                if (Result != WSMBT.Result.SUCCESS)
                {
                    MessageBox.Show(wsmbtControl1.GetLastErrorString());
                }
            });
        }
        */

    }
}
