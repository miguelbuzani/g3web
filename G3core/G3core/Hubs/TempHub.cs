﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace G3core.Hubs
{
    public class TempHub : Hub
    {
        public async Task SendMessage(string user,string message)
        {
            await Clients.All.SendAsync("ReceiveMessage",user, message);
        }

        public async Task SendHR(string user, int[] message)
        {
            //await Clients.User(user).SendAsync("ReceiveHR",user, message);
            await Clients.Others.SendAsync("ReceiveHR", user, message);
        }
    }
}
