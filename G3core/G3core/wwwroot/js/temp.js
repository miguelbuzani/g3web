﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("/tempHub")
    .configureLogging(signalR.LogLevel.Information)
    .build();


connection.on("ReceiveMessage", (user, message) => {
    var temps = message.split("\n");
    document.getElementById("HR1").innerHTML = temps[0];
    document.getElementById("HR2").innerHTML = temps[1];
    document.getElementById("HR3").innerHTML = temps[2];
    document.getElementById("HR4").innerHTML = temps[3];
    document.getElementById("HR5").innerHTML = temps[4];
    document.getElementById("HR6").innerHTML = temps[5];
    document.getElementById("HR7").innerHTML = temps[6];
    document.getElementById("HR8").innerHTML = temps[7];
    document.getElementById("HR9").innerHTML = temps[8];
    document.getElementById("HR10").innerHTML = temps[9];
});

document.getElementById("sendButton").addEventListener("click", event => {
    var user = document.getElementById("InputUSER").value;
    var hr = [];
    hr[0] = parseInt(document.getElementById("IHR1").value,10);
    hr[1] = parseInt(document.getElementById("IHR2").value,10);
    hr[2] = parseInt(document.getElementById("IHR3").value,10);
    hr[3] = parseInt(document.getElementById("IHR4").value,10);
    hr[4] = parseInt(document.getElementById("IHR5").value,10);
    hr[5] = parseInt(document.getElementById("IHR6").value,10);
    hr[6] = parseInt(document.getElementById("IHR7").value,10);
    hr[7] = parseInt(document.getElementById("IHR8").value,10);
    hr[8] = parseInt(document.getElementById("IHR9").value,10);
    hr[9] = parseInt(document.getElementById("IHR10").value, 10);
    connection.invoke("SendHR", user, hr);
});


connection.start().catch(err => console.error(err.toString()));


/*
 * Metodo para enviar al cliente
 document.getElementById("sendButton").addEventListener("click", event => {
    const user = document.getElementById("userInput").value;
    const message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(err => console.error(err.toString()));
    event.preventDefault();
});
 *
 * "ReceiveMessage pero con loop"
 connection.on("ReceiveMessage", (user, message) => {
    var hrnum = "HR";
    var temps = message.split("\n");

    for (var i = 0; i < temps.lenght; i++) {
        var temphrnum = hrnum + String(i);
        document.getElementById(temphrnum).innerHTML = temps[i];
    }
});
 */